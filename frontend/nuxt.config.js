module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'АО-ТРАНС - международные автомобильные грузоперевозки',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    // CSS file in the project
    '@/assets/css/fontawesome-all.min.css',
    '@/assets/css/icomoon.css',
    'iview/dist/styles/iview.css'
  ],
  render: {
    gzip: {}
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#034575' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: [
      'iview'
    ],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      config.module.rules.push({
        test: /\.vue$/,
        loader: 'iview-loader',
        options: {
          prefix: false
        }
      })
    }
  },
  plugins: [
    {
      src: '~/plugins/iview.js',
      ssr: false
    }
  ],
  env: {
    ENV: process.env.ENV || 'dev'
  }
}
