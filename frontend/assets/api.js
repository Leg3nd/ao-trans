import axios from 'axios'

class Api {
  constructor ({endpoint}) {
    this._endpoint = endpoint
  }

  get endpoint () {
    return this._endpoint
  }

  async fetchDrivers () {
    return this._fetchData('drivers')
  }

  async requestCall (data) {
    return this._fetchData('request-call', 'post', data)
  }

  async fetchCallRequests () {
    return this._fetchData('request-call')
  }

  async removeCallRequest (id) {
    return this._fetchData(`request-call/${id}`, 'delete')
  }

  async fetchHomeInfo () {
    return this._fetchData('home-info')
  }

  async fetchHeaderInfo () {
    return this._fetchData('header-info')
  }

  async fetchTrucks () {
    return this._fetchData('trucks')
  }

  async fetchFreeTrucks () {
    return this._fetchData('free-trucks')
  }

  async fetchTruck (id) {
    return this._fetchData(`trucks/${id}`)
  }

  async saveTruck (data) {
    return this._fetchData('trucks', 'post', data)
  }

  async removeTruck (id) {
    return this._fetchData(`trucks/${id}`, 'delete')
  }

  async fetchTrailers () {
    return this._fetchData('trailers')
  }

  async fetchFreeTrailers () {
    return this._fetchData('free-trailers')
  }

  async fetchTrailer (id) {
    return this._fetchData(`trailers/${id}`)
  }

  async saveTrailer (data) {
    return this._fetchData('trailers', 'post', data)
  }

  async removeTrailer (id) {
    return this._fetchData(`trailers/${id}`, 'delete')
  }

  async saveDriver (data) {
    return this._fetchData('drivers', 'post', data)
  }

  async removeDriver (id) {
    return this._fetchData(`drivers/${id}`, 'delete')
  }

  async sendMainInfo (data) {
    return this._fetchData('main-info', 'post', data)
  }

  async _fetchData (methodName, methodType = 'get', body = {}) {
    try {
      let res = await axios[methodType](`${this._endpoint}/${methodName}`, body)
      return res.data
    } catch (err) {
      return err.response.data
    }
  }

  async login (email, password) {
    let res = await this._fetchData('login', 'post', {email, password})
    if (res.result) {
      localStorage.setItem('token', res.result)
    } else {
      throw new Error(res.errors[0].message)
    }
  }

  logout () {
    localStorage.removeItem('token')
  }

  isLoggedIn () {
    return localStorage.getItem('token')
  }
}

let host = process.browser ? window.location.hostname : 'localhost'
let port = process.env.ENV === 'dev' ? 3001 : 80

export default new Api({
  endpoint: `http://${host}:${port}/api/frontend`
})
