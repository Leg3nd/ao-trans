class DatabaseGateway {
  constructor () {
    this._phone = '+38 066 969 29 56'
    this._address = 'ул. Куйбышева, 20-А'
    this._area = 'Балаклея, Харьковская область'
    this._workingDays = [0, 4]
    this._workingHours = ['09:00', '18:00']
    this._aboutTitle = 'Кто мы такие?'
    this._aboutDescription = `<nobr>ООО «АО-ТРАНС»</nobr> - это
    международные автомобильные грузоперевозки (TIR, CMR) и экспедиторские
    услуги, перевозка  грузов на высшем уровне в <nobr>Харьковской области</nobr>,
    Украине, России, странах СНГ и Европы. <br><br>
    Автопарк укомплектован современной техникой: тентовые полуприцепы,
    изотерм и рефрежиратор. У нас работают профессионалы, которые соблюдают
    сроки и условия доставки грузов. <br><br>
    Перевозка грузов автомобильным транспортом, является самым популярным
    способом доставки грузов в международном сообщении. Осуществляя международные
    автомобильные грузоперевозки, специалисты транспортной компании
    <nobr>ООО «АО-ТРАНС»:</nobr> логист и диспетчер, предоставят полную
    информацию о состоянии Вашего груза. Мы контролируем процесс транспортировки
    по всему маршруту следования. Такой подход обеспечивает безопасность,
    сроки доставки и оперативность транспортных услуг.`
    this._cars = [
      {
        id: 1,
        name: 'Рено Магнум',
        number: 'АХ7809ЕІ'
      },
      {
        id: 2,
        name: 'Рено Магнум',
        number: 'АХ7809ЕІ'
      },
      {
        id: 3,
        name: 'Рено Магнум',
        number: 'АХ7809ЕІ'
      },
      {
        id: 4,
        name: 'Рено Магнум',
        number: 'АХ7809ЕІ'
      }
    ]
    this._trailers = [
      {
        id: 1,
        name: 'Schmitz',
        number: 'АХ3429ХР'
      },
      {
        id: 2,
        name: 'Schmitz',
        number: 'АХ3429ХР'
      },
      {
        id: 3,
        name: 'Schmitz',
        number: 'АХ3429ХР'
      },
      {
        id: 4,
        name: 'Schmitz',
        number: 'АХ3429ХР'
      }
    ]
    this._avatars = []
  }

  async getContactPhone () {
    return this._phone
  }

  async setContactPhone (value) {
    this._phone = value
  }

  async getContactAddress () {
    return this._address
  }

  async setContactAddress (value) {
    this._address = value
  }

  async getContactArea () {
    return this._area
  }

  async setContactArea (value) {
    this._area = value
  }

  async getWorkingDays () {
    return this._workingDays
  }

  async setWorkingDays (value) {
    this._workingDays = value
  }

  async getWorkingHours () {
    return this._workingHours
  }

  async setWorkingHours (value) {
    this._workingHours = value
  }

  async getAboutTitle () {
    return this._aboutTitle
  }

  async setAboutTitle (value) {
    this._aboutTitle = value
  }

  async getAboutDescription () {
    return this._aboutDescription
  }

  async setAboutDescription (value) {
    this._aboutDescription = value
  }

  async getCars () {
    return this._cars
  }

  async setCar ({id, name, number}) {
    let carIndex = -1

    if (id) {
      for (let i = 0; i < this._cars.length; ++i) {
        if (this._cars[i].id === id) {
          carIndex = i
          break
        }
      }
    }

    if (~carIndex) {
      this._cars[carIndex].name = name
      this._cars[carIndex].number = number
      return id
    } else {
      this._cars.push({
        id: this._cars[this._cars.length - 1].id + 1,
        name,
        number
      })
      return this._cars[this._cars.length - 1].id
    }
  }

  async removeCar (id) {
    id = Number(id)
    let carIndex = -1

    if (id) {
      for (let i = 0; i < this._cars.length; ++i) {
        if (this._cars[i].id === id) {
          carIndex = i
          break
        }
      }
    }

    if (~carIndex) {
      this._cars.splice(carIndex, 1)
    }
  }

  async uploadAvatar (avatar) {
    avatar.id = this._avatars.length
      ? this._avatars[this._avatars.length - 1].id + 1
      : 1
    this._avatars.push(avatar)
    return this._avatars[this._avatars.length - 1].id
  }

  async getAvatar (id) {
    id = Number(id)
    return this._avatars.filter(a => a.id === id)[0]
  }

  async getTrailers () {
    return this._trailers
  }

  async setTrailer ({id, name, number}) {
    let trailerIndex = -1

    if (id) {
      for (let i = 0; i < this._trailers.length; ++i) {
        if (this._trailers[i].id === id) {
          trailerIndex = i
          break
        }
      }
    }

    if (~trailerIndex) {
      this._trailers[trailerIndex].name = name
      this._trailers[trailerIndex].number = number
      return id
    } else {
      this._trailers.push({
        id: this._trailers[this._trailers.length - 1].id + 1,
        name,
        number
      })
      return this._trailers[this._trailers.length - 1].id
    }
  }

  async removeTrailer (id) {
    id = Number(id)
    let trailerIndex = -1

    if (id) {
      for (let i = 0; i < this._trailers.length; ++i) {
        if (this._trailers[i].id === id) {
          trailerIndex = i
          break
        }
      }
    }

    if (~trailerIndex) {
      this._trailers.splice(trailerIndex, 1)
    }
  }

  async getDriverList () {
    return [
      {
        image: 'https://ld-wp.template-help.com/wordpress_62353/wp-content/uploads/2016/12/blog-1-150x150.jpg',
        name: 'Андреев Олег',
        phone: [
          '+38 066 969 29 56',
          '+38 066 969 29 56'
        ],
        drivingLicense: 'АХ3429ХР',
        car: {
          name: 'Рено Магнум',
          number: 'АХ7809ЕІ'
        },
        trailer: {
          name: 'Schmitz',
          number: 'АХ3429ХР'
        }
      },
      {
        image: 'https://ld-wp.template-help.com/wordpress_62353/wp-content/uploads/2016/12/blog-1-150x150.jpg',
        name: 'Котов Тимофей',
        phone: [
          '+38 066 969 29 56'
        ],
        drivingLicense: 'АХ3429ХР',
        car: {
          name: 'Рено Магнум',
          number: 'АХ7809ЕІ'
        },
        trailer: {
          name: 'Schmitz',
          number: 'АХ3429ХР'
        }
      },
      {
        image: 'https://ld-wp.template-help.com/wordpress_62353/wp-content/uploads/2016/12/blog-1-150x150.jpg',
        name: 'Котов Тимофей1',
        phone: [
          '+38 066 969 29 56'
        ],
        drivingLicense: 'АХ3429ХР',
        car: {
          name: 'Рено Магнум',
          number: 'АХ7809ЕІ'
        },
        trailer: {
          name: 'Schmitz',
          number: 'АХ3429ХР'
        }
      }
    ]
  }
}

module.exports = DatabaseGateway
