const nodemailer = require('nodemailer')

class Emailer {
  constructor ({username, password}) {
    this._username = username
    this._password = password
    this._subject = 'Новая заявка на звонок'
  }

  send (recipient, {phone, date, comment}) {
    let transporter = nodemailer.createTransport({
      host: 'smtp.sendgrid.net',
      port: 465,
      secure: true,
      auth: {
        user: this._username,
        pass: this._password
      }
    })
    let content = `<p><h3>У Вас новая заявка на звонок!</h3></p>
      <strong>Номер телефона:</strong> ${phone}<br>
      <strong>Дата и время:</strong> ${date.toLocaleString().slice(0, -3)}<br>`

    if (comment) {
      content += `<strong>Комментарий:</strong> ${comment}`
    }

    let mailOptions = {
      from: 'ao-trans@ao-trans.com',
      to: recipient,
      subject: this._subject,
      html: content
    }

    return new Promise((resolve, reject) => {
      return transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          return reject(error)
        }

        resolve(info)
      })
    })
  }
}

module.exports = Emailer
