const ObjectID = require('mongodb').ObjectID

class DataMongoStorage {
  constructor (db) {
    this._db = db
  }

  async getContactPhone () {
    return this._getGeneralData('phone')
  }

  async setContactPhone (value) {
    await this._setGeneralData('phone', value)
  }

  async getContactAddress () {
    return this._getGeneralData('address')
  }

  async setContactAddress (value) {
    await this._setGeneralData('address', value)
  }

  async getContactArea () {
    return this._getGeneralData('area')
  }

  async setContactArea (value) {
    await this._setGeneralData('area', value)
  }

  async getWorkingDays () {
    return this._getGeneralData('workingDays')
  }

  async setWorkingDays (value) {
    await this._setGeneralData('workingDays', value)
  }

  async getCallsEmail () {
    return this._getGeneralData('callsEmail')
  }

  async setCallsEmail (value) {
    await this._setGeneralData('callsEmail', value)
  }

  async getWorkingHours () {
    return this._getGeneralData('workingHours')
  }

  async setWorkingHours (value) {
    await this._setGeneralData('workingHours', value)
  }

  async getAboutTitle () {
    return this._getGeneralData('aboutTitle')
  }

  async setAboutTitle (value) {
    await this._setGeneralData('aboutTitle', value)
  }

  async getAboutDescription () {
    return this._getGeneralData('aboutDescription')
  }

  async setAboutDescription (value) {
    await this._setGeneralData('aboutDescription', value)
  }

  async getTrucks () {
    return this._getEntities('trucks')
  }

  async getFreeTrucks () {
    let drivers = await this.getDrivers()
    let usedTrucksId = drivers.filter(e => !!e.truckId).map(e => ObjectID.createFromHexString(e.truckId))

    let entities = await this._db.collection('trucks')
      .find({
        _id: {$nin: usedTrucksId}
      })
      .toArray()

    return entities.map(e => {
      e.id = e._id
      return e
    })
  }

  async getTruck (id) {
    let data = await this._db.collection('trucks').findOne({
      _id: ObjectID.createFromHexString(id)
    })

    if (data) {
      data.id = data._id
      return data
    }

    return {}
  }

  async setTruck ({id, name, number}) {
    return this._setEntity('trucks', {id, name, number})
  }

  async removeTruck (id) {
    await this._removeEntity('trucks', id)
  }

  async setAvatar ({id, name, mimetype, size, data}) {
    let drivers = await this.getDrivers()
    let usedAvatarsId = drivers.map(e => ObjectID.createFromHexString(e.avatarId))

    await this._db.collection('avatars').deleteMany({
      _id: {$nin: usedAvatarsId}
    })

    return this._setEntity('avatars', {id, name, mimetype, size, data})
  }

  async getAvatar (id) {
    let data = await this._db.collection('avatars').findOne({
      _id: ObjectID.createFromHexString(id)
    })

    if (data) {
      data.id = data._id
      data.data = data.data.buffer
      return data
    }

    return {}
  }

  async getTrailers () {
    return this._getEntities('trailers')
  }

  async getFreeTrailers () {
    let drivers = await this.getDrivers()
    let usedTrailersId = drivers.filter(e => !!e.trailerId).map(e => ObjectID.createFromHexString(e.trailerId))

    let entities = await this._db.collection('trailers')
      .find({
        _id: {$nin: usedTrailersId}
      })
      .toArray()

    return entities.map(e => {
      e.id = e._id
      return e
    })
  }

  async getTrailer (id) {
    let data = await this._db.collection('trailers').findOne({
      _id: ObjectID.createFromHexString(id)
    })

    if (data) {
      data.id = data._id
      return data
    }

    return {}
  }

  async getUser (email) {
    let data = await this._db.collection('users').findOne({email})

    if (data) {
      data.id = data._id
      return data
    }

    return null
  }

  async setTrailer ({id, name, number}) {
    return this._setEntity('trailers', {id, name, number})
  }

  async removeTrailer (id) {
    await this._removeEntity('trailers', id)
  }

  async getCallRequests () {
    return this._getEntities('calls', {
      sort: {
        status: -1,
        date: -1
      }
    })
  }

  async setCallRequest ({id, phone, status, date, comment}) {
    return this._setEntity('calls', {id, phone, status, date, comment})
  }

  async removeCallRequest (id) {
    await this._removeEntity('calls', id)
  }

  async getDrivers () {
    return this._getEntities('drivers')
  }

  async setDriver ({id, avatarId, name, drivingLicense, phone, truckId, trailerId}) {
    return this._setEntity('drivers', {id, avatarId, name, drivingLicense, phone, truckId, trailerId})
  }

  async removeDriver (id) {
    await this._removeEntity('drivers', id)
  }

  async _setGeneralData (key, value) {
    return this._db.collection('general').updateOne({}, {
      $set: {
        [key]: value
      }
    }, {
      upsert: true
    })
  }

  async _getGeneralData (key) {
    let data = await this._db.collection('general').findOne()

    if (data) {
      return data[key]
    }

    return ''
  }

  async _removeEntity (collection, id) {
    return this._db.collection(collection).deleteOne({
      _id: id && ObjectID.createFromHexString(id)
    })
  }

  async _getEntities (collection, {sort = {}} = {}) {
    let entities = await this._db.collection(collection)
      .find()
      .sort(sort)
      .toArray()

    return entities.map(e => {
      e.id = e._id
      return e
    })
  }

  async _setEntity (collection, {id, ...args}) {
    let res = await this._db.collection(collection).updateOne({
      _id: (id && ObjectID.createFromHexString(id)) || new ObjectID()
    }, {
      $set: args
    }, {
      upsert: true
    })

    if (res.result.upserted) {
      return res.result.upserted[0]._id
    }
  }
}

module.exports = DataMongoStorage
