class Util {
  static promiseAll (o) {
    let target = Object.keys(o).reduce((r, k) => {
      r.keys.push(k)
      r.values.push(o[k])

      return r
    }, {keys: [], values: []})

    return new Promise((resolve) => {
      Promise.all(target.values).then(res => {
        let result = res.reduce((r, e, i) => {
          r[target.keys[i]] = e
          return r
        }, {})

        resolve(result)
      })
    })
  }
}

module.exports = Util
