const config = require('./config')
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const httpProxy = require('http-proxy')
const Util = require('./util.js')
// const DataGateway = require('./database-gateway')
const DataMongoStorage = require('./data-mongo-storage')
const Emailer = require('./emailer.js')
const argon2 = require('argon2')
const jwt = require('jsonwebtoken')
const MongoClient = require('mongodb').MongoClient
const multer = require('multer')
const upload = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 1000000 * 2 // 2MB
  }
})
// const path = require('path')

async function run () {
  let client = await MongoClient.connect(config.db.url, {useNewUrlParser: true})
  let db = client.db(config.db.name)

  await db.collection('users').updateOne({
    email: config.admin.email
  }, {
    $set: {
      email: config.admin.email,
      passwordHash: await argon2.hash(config.admin.password),
      token: await jwt.sign({
        email: config.admin.email
      }, config.admin.secret, {
        expiresIn: '10d', algorithm: 'HS384'
      })
    }
  }, {
    upsert: true
  })

  // const dataGateway = new DataGateway()
  const dataGateway = new DataMongoStorage(db)
  const emailer = new Emailer(config.emailer)
  const app = express()
  const proxy = httpProxy.createProxyServer()

  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(morgan('tiny'))

  app.use('/', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    res.header('Access-Control-Allow-Headers', 'Content-Type')

    next()
  })

  let optionsHandler = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    res.header('Access-Control-Allow-Headers', 'Content-Type')

    res.end()
  }

  app.options('/api/frontend/trucks/:id', optionsHandler)
  app.options('/api/frontend/trucks/', optionsHandler)
  app.options('/api/frontend/trailers/:id', optionsHandler)
  app.options('/api/frontend/trailers/', optionsHandler)
  app.options('/api/frontend/upload-avatar/', optionsHandler)
  app.options('/api/frontend/drivers/', optionsHandler)
  app.options('/api/frontend/drivers/:id', optionsHandler)
  app.options('/api/frontend/request-call', optionsHandler)
  app.options('/api/frontend/request-call/:id', optionsHandler)
  app.options('/api/frontend/login', optionsHandler)

  app.post('/api/frontend/login', async (req, res) => {
    let {email, password} = req.body
    let user = await dataGateway.getUser(email)

    if (user) {
      let isVerified = await argon2.verify(user.passwordHash, password)

      if (isVerified) {
        res.json({result: user.token})
        res.end()
        return
      }
    }

    res.status(422).json({
      errors: [
        {field: 'email', message: 'Wrong email or password'},
        {field: 'password', message: 'Wrong email or password'}
      ]
    })
    res.end()
  })

  app.post('/api/frontend/request-call', async (req, res) => {
    let {id, phone, timezoneOffset, date = new Date(), comment = '', status = 'NEW'} = req.body
    date = new Date(date)

    let hoursOffset = timezoneOffset ? (-180 - timezoneOffset) / 60 : 0

    if (Math.abs(hoursOffset) > 0) {
      comment = `У клиента ${hoursOffset > 0 ? '+' : '-'}${hoursOffset} ч. от вашего времени.`
    }

    let recipient = await dataGateway.getCallsEmail()
    await emailer.send(recipient, {phone, date, comment})

    await dataGateway.setCallRequest({id, phone, date, comment, status})

    res.end()
  })

  app.get('/api/frontend/request-call', async (req, res) => {
    let callReq = await dataGateway.getCallRequests()
    res.json(callReq)
    res.end()
  })

  app.delete('/api/frontend/request-call/:id', async (req, res) => {
    await dataGateway.removeCallRequest(req.params.id)
    res.end()
  })

  app.get('/api/frontend/header-info', async (req, res) => {
    let headerInfo = await Util.promiseAll({
      phone: dataGateway.getContactPhone(),
      address: dataGateway.getContactAddress(),
      area: dataGateway.getContactArea(),
      workingDays: dataGateway.getWorkingDays(),
      workingHours: dataGateway.getWorkingHours()
    })
    res.send(headerInfo)
    res.end()
  })

  app.get('/api/frontend/drivers', async (req, res) => {
    let drivers = await dataGateway.getDrivers()
    res.send(drivers)
    res.end()
  })

  app.use('/api/frontend/main-info', async (req, res) => {
    let handlers = {
      phone: dataGateway.setContactPhone.bind(dataGateway),
      address: dataGateway.setContactAddress.bind(dataGateway),
      area: dataGateway.setContactArea.bind(dataGateway),
      workingDays: dataGateway.setWorkingDays.bind(dataGateway),
      workingHours: dataGateway.setWorkingHours.bind(dataGateway),
      aboutTitle: dataGateway.setAboutTitle.bind(dataGateway),
      aboutDescription: dataGateway.setAboutDescription.bind(dataGateway)
    }
    await Object.keys(req.body).map(key => handlers[key](req.body[key]))
    res.end()
  })

  app.get('/api/frontend/home-info', async (req, res) => {
    let homeInfo = await Util.promiseAll({
      aboutTitle: dataGateway.getAboutTitle(),
      aboutDescription: dataGateway.getAboutDescription()
    })
    res.send(homeInfo)
    res.end()
  })

  app.get('/api/frontend/trucks', async (req, res) => {
    let cars = await dataGateway.getTrucks()
    res.send(cars)
    res.end()
  })

  app.get('/api/frontend/free-trucks', async (req, res) => {
    let cars = await dataGateway.getFreeTrucks()
    res.send(cars)
    res.end()
  })

  app.get('/api/frontend/trucks/:id', async (req, res) => {
    let truck = await dataGateway.getTruck(req.params.id)
    res.send(truck)
    res.end()
  })

  app.post('/api/frontend/trucks', async (req, res) => {
    let id = await dataGateway.setTruck(req.body)
    res.json(id)
    res.end()
  })

  app.delete('/api/frontend/trucks/:id', async (req, res) => {
    await dataGateway.removeTruck(req.params.id)
    res.end()
  })

  app.get('/api/frontend/trailers', async (req, res) => {
    let trailers = await dataGateway.getTrailers()
    res.send(trailers)
    res.end()
  })

  app.get('/api/frontend/free-trailers', async (req, res) => {
    let cars = await dataGateway.getFreeTrailers()
    res.send(cars)
    res.end()
  })

  app.get('/api/frontend/trailers/:id', async (req, res) => {
    let trailer = await dataGateway.getTrailer(req.params.id)
    res.send(trailer)
    res.end()
  })

  app.post('/api/frontend/trailers', async (req, res) => {
    let id = await dataGateway.setTrailer(req.body)
    console.log(id)
    res.json(id)
    res.end()
  })

  app.delete('/api/frontend/trailers/:id', async (req, res) => {
    await dataGateway.removeTrailer(req.params.id)
    res.end()
  })

  app.post('/api/frontend/upload-avatar', upload.array('file'), async (req, res) => {
    let id = await dataGateway.setAvatar({
      mimetype: req.files[0].mimetype,
      name: req.files[0].originalname,
      data: req.files[0].buffer,
      size: req.files[0].size
    })
    res.json(id)
    res.end()
  })

  app.get('/api/frontend/avatar/:id', async (req, res) => {
    let avatar = await dataGateway.getAvatar(req.params.id)
    res.send(avatar.data)
    res.end()
  })

  app.post('/api/frontend/drivers', async (req, res) => {
    let id = await dataGateway.setDriver(req.body)
    res.json(id)
    res.end()
  })

  app.delete('/api/frontend/drivers/:id', async (req, res) => {
    await dataGateway.removeDriver(req.params.id)
    res.end()
  })

  app.use('/', (req, res) => {
    proxy.web(req, res, {
      target: `http://localhost:${3000}`
    }, (e) => {
      console.log(e)
      res.statusCode = 500
      res.end()
    })
  })

  app.listen(process.env.PORT || 3001, () => {
    console.log('Server started on port: ', process.env.PORT || 3001)
  })
}

run()
